/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    StyleSheet,
    SafeAreaView
} from 'react-native';

import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';
import  Root from './app/views/general/Root'
import {Provider} from 'react-redux';
import configureStore from "./app/redux/configureStore";
const store = configureStore();


if(__DEV__) {
    import('./app/utilities/ReactotronConfig').then(() => console.log('Reactotron Configured'))
}
const App: () => React$Node = () => {
    return (
        <Provider store={store}>
        <SafeAreaView style={{flex:1}}>
           <Root/>
        </SafeAreaView>
        </Provider>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});

export default App;
