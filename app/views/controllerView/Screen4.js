import React, {useEffect, useRef, useState} from 'react';
import {Button, Text, View, Animated, Easing, Pressable} from 'react-native';

const Screen4 = ({navigation}) => {
    const [seconds, setSeconds] = useState(0);
    const [isActive, setIsActive] = useState(true);
    let interval = useRef().current;


    const getRandomColor = () => {
        return 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
    };
    useEffect(() => {

        if (isActive) {

            interval = setInterval(() => {
                setSeconds(seconds => seconds + 1);
            }, 1000);
        } else {
            clearInterval(interval);
            setSeconds(0);
        }
        return () => clearInterval(interval);

    }, [seconds, isActive]);

    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'pink'}}>
            <Text>{isActive ? 'ACTIVE' : 'NOTACTIVE'}</Text>
            <View style={{
                height: 200,
                width: 400,
                borderColor: getRandomColor(),
                backgroundColor: getRandomColor(),
                borderStyle: 'solid',
                borderWidth: 20,
                justifyContent: 'center',
            }}>
                <Text style={{textAlign: 'center'}}>{seconds}</Text>
            </View>

            <Pressable style={{
                height: 50,
                width: 100,
                backgroundColor: 'green',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10,
            }} onPress={() => {
                setIsActive(!isActive);
            }}>
                {({pressed}) => (
                    <Text style={{}}>
                        {pressed ? 'Pressed!' : 'Press Me'}
                    </Text>
                )}
            </Pressable>

        </View>
    );
};
export default Screen4;
