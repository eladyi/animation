import React, {useEffect, useRef, useState, useMemo, useCallback} from 'react';
import {Button, Text, View, Animated, Easing, Pressable} from 'react-native';

const Screen4 = ({navigation}) => {


   // let arr = [...Array(10000).keys()];
    let arr = [...Array(10).keys()];


   // console.time("for");
   // console.log(arr);
    console.time("for")
    for (let i = 0; i < arr.length; i++) {
        {
            console.log(arr[i],"for")
        }
    }
    console.timeEnd("for");

    console.time("forEach")
    arr.forEach(x=>{
        console.log(x,"forEach")
    })
    console.timeEnd("forEach");

    console.time("of")
    for(let aaa of arr)
    {
        console.log(aaa,"of")
    }
    console.timeEnd("of");


    console.time("in")
    for(let aaa in arr)
    {
        console.log(arr[aaa],"in")
    }
    console.timeEnd("in");
   // useMemo(expensiveFunction, dependencyArray)

    const fetchData = async (id) => {
        let res = await fetch('https://jsonplaceholder.typicode.com/todos/' + id);
        return await res.json();
    };

    const multiply = (num) =>
    {
        console.log(num,'...multiply');
      return   num*num;
    }


    const [num, setNum] = useState(0);
    const [data, setData] = useState({});
    const [refresh, setRefresh] = useState(0);

    const handleClick = useCallback(fetchData, [num]);
   // const handleMemo =  useMemo(()=> multiply(data), )

    const handleMemo =  useMemo(async ()=> fetchData(num),[num] )

    // useEffect(() => {
    //      handleClick(num).then(x=>{
    //           setData(x);
    //      })
    // },[handleClick]);

    // useEffect(() => {
    //      handleClick(num).then(x=>{
    //           setData(x);
    //      })
    // },[handleClick]);


    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'pink'}}>

            <Pressable style={{
                height: 50,
                width: 100,
                backgroundColor: 'green',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10,
            }} onPress={() => {
                setNum(Math.floor(Math.random() * 10));
                //setNum(1);
            }}>
                {({pressed}) => (
                    <Text style={{}}>
                        {pressed ? 'Pressed!' : 'Press Me'}
                    </Text>
                )}
            </Pressable>


            <Pressable style={{
                height: 50,
                width: 100,
                backgroundColor: 'green',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10,
            }} onPress={() => {
                setRefresh(Math.floor(Math.random() * 10));
                //setNum(1);
            }}>
                {({pressed}) => (
                    <Text style={{}}>
                        {pressed ? 'refresh!' : 'refresh Me'}
                    </Text>
                )}
            </Pressable>

            <Text>{JSON.stringify(data)}</Text>

            <Text>{JSON.stringify(handleMemo.then((x)=>{console.log(JSON.stringify(x))}))}</Text>
        </View>
    );
};
export default Screen4;
