import React,{useEffect,useRef,useState} from 'react';
import {Button, Text, View,Animated,Easing} from 'react-native';

const Screen2 = ({ navigation }) => {
    const top = useRef(new Animated.Value(0)).current
    const [toggleState, setToggleState] = useState(false);

    useEffect(() => {
        Animated.timing(
            top,
            {
                toValue: toggleState?50:-50,
                duration: 2000,
                useNativeDriver:true,
                easing: Easing.in((Easing.elastic(0.5)))
            }
        ).start(()=>{setToggleState(!toggleState)});
    }, [toggleState])



    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor:'pink' }}>

            <View style={{height:200,width:400,borderColor:'green',borderStyle:'solid',borderWidth:1,justifyContent:'center'}}>

                <Animated.View style={{height:1,width:"100%",backgroundColor:'red',transform: [{translateY: top}]}}></Animated.View>

            </View>

        </View>
    );
};
export  default Screen2;
