import React, { useRef,useEffect } from "react";
import {Button, View,Text,Pressable,Animated,PanResponder} from 'react-native';

const Screen1 = ({ navigation }) => {
    const pan = useRef(new Animated.ValueXY()).current;

    const panResponder = useRef(
        PanResponder.create({
            onMoveShouldSetPanResponder: () => true,
            onPanResponderGrant: () => {
                pan.setOffset({
                    x: pan.x._value,
                    y: pan.y._value
                });
            },
            onPanResponderMove: Animated.event(
                [
                    null,
                    { dx: pan.x, dy: pan.y },
                ],{useNativeDriver: false}
            ),
            onPanResponderRelease: () => {
                pan.flattenOffset();
            }
        })
    ).current;


    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor:"green" }}>

          <Animated.View   {...panResponder.panHandlers}  style={{
              transform: pan.getTranslateTransform()
          }}>
            <Pressable  onPress={()=>{alert("Pressable pressed")}} style={{height:50,width:50,backgroundColor:'red',borderRadius:25}}>

            </Pressable>
          </Animated.View>
        </View>
    );
};
export  default Screen1;
