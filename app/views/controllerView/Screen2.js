import React,{useEffect,useRef,useState} from 'react';
import {Button, Text, View,Animated} from 'react-native';


const Screen2 = ({ navigation }) => {

    const fadeAnim = useRef(new Animated.Value(0)).current
    const [toggleState, setToggleState] = useState(false);


    useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue: toggleState?1:0,
                duration: 3000,
                useNativeDriver: true
            }
        ).start(()=>{setToggleState(!toggleState)});
    }, [toggleState])



    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor:'pink' }}>

          <Animated.View style={{height:100,width:100,backgroundColor:'blue', opacity: fadeAnim}}></Animated.View>

        </View>
    );
};
export  default Screen2;
