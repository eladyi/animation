import React from 'react';
import {Button, Text, View} from 'react-native';
import helper from './../../utilities/helper'

const Drawer = ({ navigation }) => {

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor:'brown' }}>
             <Text onPress={()=>{navigation.navigate('Drawer', { screen: 'Screen1' })}}>screen1</Text>
             <Text onPress={()=>{navigation.navigate('Drawer', { screen: 'Screen2' })}} >screen2</Text>
             <Text onPress={()=>{navigation.navigate('Drawer', { screen: 'Screen3' })}} >screen3</Text>
             <Text onPress={()=>{navigation.navigate('Drawer', { screen: 'Screen4' })}} >screen4</Text>
             <Text onPress={()=>{navigation.navigate('Drawer', { screen: 'Screen5' })}} >screen5</Text>
             <Text onPress={()=>{navigation.navigate('Drawer', { screen: 'Screen6' })}} >screen6</Text>
             <Text onPress={()=>{navigation.toggleDrawer()} }>toggleDrawer</Text>
             <Text onPress={()=>{helper.toggleDrawer()} }>toggleDrawerHelper</Text>
        </View>
    );
};
export  default Drawer;
