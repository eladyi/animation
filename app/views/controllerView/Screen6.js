import React, {useEffect, useRef, useState} from 'react';
import {Button, Text, View, Animated, Easing, Pressable} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import * as CoreActionCreator from "./../../redux/ActionCreator/coreActionCreator";
import  { navigate } from './../../utilities/HelperNavigation';

const Screen6 = ({navigation}) => {
    const core = useSelector(state => state.core);
    const dispatch = useDispatch();

    const setCounter =(counter)=>{
        dispatch(CoreActionCreator.setCount(counter))
    }


    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'pink'}}>

            <View style={{
                height: 200,
                width: 400,
                borderColor: 'green',
                borderStyle: 'solid',
                borderWidth: 1,
                justifyContent: 'center',
            }}>
                <Text style={{textAlign: 'center'}}>{core.counter}</Text>
            </View>


            <Pressable style={{
                height: 50,
                width: 100,
                backgroundColor: 'green',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10,
            }} onPress={() => { setCounter(core.counter +1);

                //setNum(1);
            }}>
                {({pressed}) => (
                    <Text style={{}}>
                        {pressed ? 'Pressed!' : 'Press Me'}
                    </Text>
                )}
            </Pressable>



            <Pressable style={{
                height: 50,
                width: 100,
                backgroundColor: 'green',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 10,
            }} onPress={() => { navigate('Drawer', { screen: 'Screen1' });

            }}>
                {({pressed}) => (
                    <Text style={{}}>
                        {pressed ? 'navigate!' : 'navigate Me'}
                    </Text>
                )}
            </Pressable>

        </View>
    );
};
export default Screen6;
