import React,{useRef} from 'react';
import {
    View,
    Text,

} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Screen1 from '../controllerView/Screen1';
import Screen2 from '../controllerView/Screen2';
import Screen3 from '../controllerView/Screen3';
import Screen4 from '../controllerView/Screen4';
import Screen5 from '../controllerView/Screen5';
import Screen6 from '../controllerView/Screen6';
import  { navigationRef } from './../../utilities/HelperNavigation';
import DrawerComponent from '../controllerView/Drawer';
import Header from '../controllerView/Header';

const Root = () => {

    const stackNav =() => {
        const Stack = useRef(createStackNavigator()).current;

        return (
            <Stack.Navigator initialRouteName="Screen1" headerMode={'screen'}
                             screenOptions={{
                                 header: ({scene, previous, navigation}) => {
                                     const {options} = scene.descriptor;
                                     let title =
                                         options.headerTitle !== undefined
                                             ? options.headerTitle
                                             : options.title !== undefined
                                             ? options.title
                                             : scene.route.name;

                                     return (
                                         <Header props={{scene, previous, navigation}} title={title}/>
                                     );
                                 },
                             }}

            >

                <Stack.Screen name="Screen1" component={Screen1}  options={{title:"Screen1 CUSTOM"}}/>
                <Stack.Screen name="Screen2" component={Screen2}  options={{title:"Screen2 CUSTOM"}} />
                <Stack.Screen name="Screen3" component={Screen3}  options={{title:"Screen3 CUSTOM"}}  />
                <Stack.Screen name="Screen4" component={Screen4}  options={{title:"Screen4 CUSTOM"}}  />
                <Stack.Screen name="Screen5" component={Screen5}  options={{title:"Screen5 CUSTOM"}}  />
                <Stack.Screen name="Screen6" component={Screen6}  options={{title:"Screen6 CUSTOM"}}  />

            </Stack.Navigator>
        );
    }

    const Drawer = createDrawerNavigator();
    return (
        <NavigationContainer ref={navigationRef}>
            <Drawer.Navigator drawerStyle={{width: '90%'}} drawerType={'front'}
                              drawerContent={(props) => <DrawerComponent {...props} />}>
                <Drawer.Screen name="Drawer" component={stackNav}/>
            </Drawer.Navigator>
        </NavigationContainer>

    );
};

export default Root;
