import keyMirror from 'keymirror';

export default Object.assign({}, keyMirror({
    SET_COUNTER: null,
}));

