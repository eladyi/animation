import {createStore, applyMiddleware} from 'redux';
import rootReducer from './rootReducer';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';

let defaultStore;

export default function configureStore() {

        const createDebugger = require('redux-flipper').default;
       const createFlipperMiddleware = require('rn-redux-middleware-flipper').default





    if (!defaultStore)
        defaultStore = createStore(
            rootReducer,
            {},
            composeWithDevTools(applyMiddleware(thunk, reduxImmutableStateInvariant(),createDebugger(),createFlipperMiddleware()))
        );
    return defaultStore;
}
