import {combineReducers} from 'redux';
import core from "./reducer/coreReducer";

const rootReducer = combineReducers({
    core,
});

export default rootReducer;
