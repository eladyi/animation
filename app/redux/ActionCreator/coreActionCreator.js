import React from 'react';
import ActionTypes from "./../../const/ActionType";

import configureStore from "./../configureStore";

const store = configureStore();

export function setCount(counter) {
    return {type: ActionTypes.SET_COUNTER, counter};
}

export function setCounterToStore(counter) {
    return (dispatch) => {
        dispatch(setCount(counter));
    }
}

