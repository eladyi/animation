import ActionTypes from './../../const/ActionType';
export default function coreReducer(state = {counter:0}, action) {
    switch (action.type) {
        case ActionTypes.SET_COUNTER: {
            return {...state,counter:action.counter};
        }
        default:
            return state;
    }
}
